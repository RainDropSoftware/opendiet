from django.db import models
from django.contrib.auth.models import User

class UserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    metric = models.BooleanField(default='True')
    weight = models.DecimalField(decimal_places=1, max_digits=4)
    height = models.IntegerField()
    birthday = models.DateField(null=True, blank=True, default=None)
    public = models.BooleanField(default='True')
    beta = models.BooleanField(default='False')

