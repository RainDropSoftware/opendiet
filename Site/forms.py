from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from Profile.models import UserInfo

CHOICE = [(True,'Kg'), (False,'Lb')]
class RegisterForm(UserCreationForm):
    metric = forms.ChoiceField(label="Kg/Lbs", choices=CHOICE, initial=1)
    weight = forms.DecimalField(label="Weight")
    height = forms.IntegerField(label="Height")
    birthday = forms.DateField(label="Birthday", required=False)
 
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'metric', 'weight', 'height', 'birthday']

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.clean_password2())
        if commit:
            user.save()
        userinfo = UserInfo(user=user)
        
        userinfo.metric = (self.cleaned_data['metric'] == 'True')
        userinfo.weight = self.cleaned_data['weight']
        userinfo.height = self.cleaned_data['height']
        userinfo.birthday = self.cleaned_data['birthday']
        if commit:
            userinfo.save()
        return user
