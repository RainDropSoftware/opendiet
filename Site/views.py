from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib.auth import authenticate, login, logout
from .forms import RegisterForm
import pprint

def home(request):
	if request.user.is_authenticated:
		return HttpResponseRedirect('overview')
	else:
		return render(request, 'home.html')


def register(request):
    pp = pprint.PrettyPrinter(indent=4)
    if( request.user.is_authenticated ):
        return HttpResponseRedirect("/log/")
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user = authenticate(username=request.POST['username'],
                                    password=request.POST['password1'])
            login(request, new_user)
            return HttpResponseRedirect("/log/")
        else:
            pp.pprint("Form Invalid")
    else:
        form = RegisterForm()
    return render(request, "registration/register.html", {
        'form': form,
    })


def beta(request):
    """Beta Message Page blocking registration"""
    return render_to_response('registration/beta.html', RequestContext(request))


def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to a success page.
            return HttpResponseRedirect("/log/")
        else:
            return HttpResponseRedirect("/expired/")
            # Return a 'disabled account' error message
    else:
        # Return an 'invalid login' error message.
        return HttpResponseRedirect("/expired/")

def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")

