from django.shortcuts import render, HttpResponse
from django.contrib.auth.decorators import login_required
from Profile.models import UserInfo
from Log.models import Log

@login_required
def overview(request):
    """Overview, displays general info about diet"""
    # Grab userinfo
    userinfo = UserInfo.objects.get(user=request.user)
    try:
        last_log = Log.objects.order_by('-log_date').filter(user=request.user)[0:1].get()
    except Log.DoesNotExist:
        return render(request, 'no_log_data.html' )
        
    return render(request, 'overview.html', {'day_7': last_log.calc_average(7),
                                             'day_30': last_log.calc_average(30),
                                             'day_90': last_log.calc_average(90),
                                             'metric': userinfo.metric})

