from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from Log.forms import LogEntry
from Log.models import Log
import datetime
import pprint

@login_required
def fitness_form(request, page="1"):
    pp = pprint.PrettyPrinter(indent=4)
    if request.method == 'POST': # If the form has been submitted...
        if 'delete' in request.POST:
            entry = Log.objects.get(id=request.POST['id'])
            entry.delete()
            return HttpResponseRedirect('/log/') # Redirect after POST
        pp.pprint("Form is POST and not delete")
        data = request.POST.copy()
        if data['body_fat'] == '':
            pp.pprint("Body fat not set")
            data['body_fat'] = float(0.0)
        else:
            pp.pprint(data['body_fat'])
            data['body_fat'] = float(data['body_fat'])
        if data['body_fat'] > 0.0:
            data['body_fat'] = float(data['body_fat'])/100
        form = LogEntry(data)  # A form bound to the POST data
        pp.pprint("Copied data into form")
        if form.is_valid():  # All validation rules pass
            pp.pprint("Form Is Valid")
            # Process the data in form.cleaned_data
            # ...
            # Pull yesterdays trend
            # If no data set trend = form.trend
            # Update future points
            
            # Check if object already exists
            try:
                logged = Log.objects.get(log_date=form.cleaned_data['log_date'],
                                         user = request.user)
            except Log.DoesNotExist:
                logged = Log(log_date=form.cleaned_data['log_date'], user = request.user)
                
            logged.weight = form.cleaned_data['weight']
            logged.body_fat = form.cleaned_data['body_fat']
            logged.note = form.cleaned_data['note']
            logged.user_inputed = True
            pp.pprint(logged)
            logged.save(update_trend=True)
            
            return HttpResponseRedirect('/log/')  # Redirect after POST
        else:
            pp.pprint("Form Invalid")
            pp.pprint(form)
    else:
        pp.pprint("Form is not POST")
        form = LogEntry()  # An unbound form

    log = Log.objects.order_by('-log_date').filter(user=request.user)
    pages = Paginator(log, 10)
    
    # ensure that we don't try for too many pages, or a negative page
    page = int(page)
    if page > pages.num_pages:
        page = pages.num_pages
    elif page < 1:
        page = 1

    current_page = pages.page(page)
    if log.count() == 0:
        next_date = datetime.date.today
        last_weight = 0.0
        last_body_fat = 0.0
    else:
        next_date = log[0].log_date + datetime.timedelta(days=1)
        last_weight = log[0].weight
        last_body_fat = float(0.0)
    return render(request, 'log.html', {
        'form': form,
        'log': current_page.object_list,
        'pages': pages,
        'current_page': current_page,
        'next_date': next_date,
        'last_weight': last_weight,
        'last_body_fat': last_body_fat,
    })

