from django.urls import path, re_path

from . import views

urlpatterns = [
	re_path(r'^$', views.fitness_form, name='log'),
	re_path(r'^(?P<page>\d+)$', views.fitness_form, name='log'),
]
