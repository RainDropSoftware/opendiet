from django.db import models
from django.contrib.auth.models import User
from Profile.models import UserInfo
from numpy import polyfit
import pprint
import datetime
from decimal import *


# Create your models here.
class Log(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    log_date = models.DateField(blank=False, null=False)
    weight = models.DecimalField(decimal_places=1, max_digits=4)
    trend = models.DecimalField(decimal_places=2, max_digits=5)
    body_fat = models.DecimalField(decimal_places=3, max_digits=3, default=0)
    note = models.TextField(blank=True)
    user_inputed = True

    def _get_lean_mass(self): 
        "Returns lean mass"
        if self.body_fat == 0.0:
            return "No Entry"
        return "%.2f" % ((1 - self.body_fat) * self.trend)
    lean_mass = property(_get_lean_mass)
    
    class Meta:
        ordering = ['user','log_date', 'weight', 'trend', 'body_fat' ]
        unique_together = ['user', 'log_date']
    
    def save(self, update_trend = False, *args, **kwargs):
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint("Save Log")
        if self.body_fat > 1:
            self.body_fat = self.body_fat
        try:
            yesterday = Log.objects.get(log_date = (self.log_date + datetime.timedelta(days=-1)), user = self.user)
        # set yesterday to self if no yesterday
        except Log.DoesNotExist:
            # Check if there is a previous day and use that instead
            try:
                last_previous_day = Log.objects.filter(log_date__lt = self.log_date, user = self.user).order_by('-log_date')[0:1].get()
                # fill in days here if update_trend = True
                yesterday = self.fill_entries(last_previous_day, self)[-1]
            # all else has failed, we are the first day, use self as yesterday
            except Log.DoesNotExist:
                yesterday = self
                self.trend = self.weight
        # Now that we have yesterday lets move on, we need to calculate the average using
        # An exponential running average with 10% smoothing
        self.trend = self.calc_trend(yesterday) 
        super(Log, self).save(*args, **kwargs)

        # if we should not update the trend we exit here
        if not update_trend:
            return

        # grab tomorrow
        tomorrow = self
        tomorrow_trend = Decimal("0.00")
        
        while not tomorrow_trend == tomorrow.trend :
            try:
                tomorrow = Log.objects.get(log_date = (tomorrow.log_date + datetime.timedelta(days=1)), user = self.user)
            except Log.DoesNotExist:
                # check for future tomorrow
                try:
                    # if there is a future date call fill_entries
                    tomorrow = Log.objects.filter(log_date__gt = (tomorrow.log_date), user = self.user)[0:1].get()
                    # save future tomorrow, and exit
                    tomorrow.save( update_trend = True, *args, **kwargs)
                    return
                except Log.DoesNotExist:
                    return
            tomorrow_trend = tomorrow.trend.quantize(Decimal('.01'), rounding=ROUND_UP)
            tomorrow.save( *args, **kwargs)

    def delete(self, *args, **kwargs):
        # check if there is a next day
        try:
            tomorrow = Log.objects.filter(log_date__gt = self.log_date, user = self.user)[0:1].get()
            super(Log, self).delete(*args, **kwargs)
            tomorrow.save(update_trend=True)
        except Log.DoesNotExist:
            super(Log, self).delete(*args, **kwargs)

    def calc_trend(self, yesterday):
        todays_weight = self.weight
        yester_weight = yesterday.trend
        trend = Decimal(Decimal(0.1)*(todays_weight - yester_weight) + yester_weight)
        return trend.quantize(Decimal('.01'), rounding=ROUND_UP)

    def fill_entries(self, first_day, last_day):
        """Returns list of entries in between first_day and last_day"""
        logs = []
        weight_delta = last_day.weight - first_day.weight
        days = (last_day.log_date - first_day.log_date ).days
        increment = Decimal(weight_delta/days)
              
        # Begin filling in dates
        i=1
        while i < days:
            second_day = Log(log_date=(first_day.log_date + datetime.timedelta(days=i)), user = self.user)

            # To produce a smoother line use more decimal places on generated amounts
            second_day.user_inputed = False
            second_day.weight = (first_day.weight + (increment*i)).quantize(Decimal('.001'), rounding=ROUND_UP)
            if i == 1:
                second_day.trend = second_day.calc_trend(first_day)
            else :
                second_day.trend = second_day.calc_trend(logs[-1])
            i = i + 1
            logs.append(second_day)
        return logs

    def entries_between_dates(self, first_day, last_day):
        """Returns list of entries in between first_day and last_day using generated entries if need be"""
        days = ( last_day - first_day ).days
        
        entries = Log.objects.order_by('log_date').filter(user=self.user, log_date__gte = first_day, log_date__lte = last_day)
       
        # Now lets iterate through the list to verify that all dates are filled.
        yesterday = entries[0]
        logs = []
        for entry in entries:
            # First put in our current log
            if (entry.log_date - yesterday.log_date ).days > 1:
                logs.extend(yesterday.fill_entries(yesterday,entry))
            logs.append(entry)
            yesterday = entry
        
        # Check if days back lands between user-inputed days
        if len(logs) < (days + 1):
            # Check for previous day with return of 1
            try:
                log = Log.objects.order_by('-log_date').filter(user=self.user, log_date__lt = first_day)[0:1].get()
            except Log.DoesNotExist:
                # If no previous days then we have all days possible
                pass
            # If there are previous days then calc and extract days - logs.count() and prepend to logs
            else:
                needed_days = (days - len(logs))
                temp_logs = log.fill_entries(log,logs[0])[:needed_days]
                temp_logs.extend(logs)
                logs = temp_logs

        return logs
    
    def calc_average(self, days):
        # Grab userinfo
        userinfo = UserInfo.objects.get(user=self.user)
        
        data = self.entries_between_dates( self.log_date - datetime.timedelta(days=days), self.log_date )
        if len(data) == 1:
            return {'weight_delta': 0, 'calories': 0, 'up_or_down': False}
        # get the best fit slope
        i = 0
        xdata = []
        ydata = []
        while i < len(data):
            xdata.append(i)
            ydata.append(float(data[i].trend))
            i=i+1
        temp = polyfit(x=xdata, y=ydata, deg=1)
        slope = temp[0]
        weight_delta = round(temp[0]*len(data),1)
        calories = abs(round(slope * (7716 if userinfo.metric else 3500 )))
        if weight_delta > 0:
            up_or_down = True
        else:
            up_or_down = False
            weight_delta = weight_delta * -1
        
        averages = {'weight_delta': weight_delta, 'calories': calories, 'up_or_down': up_or_down}
        
        return averages

