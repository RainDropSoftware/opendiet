from django import template

register = template.Library()

@register.filter
def percentage(value):
    return format(value, "%")

@register.filter
def percent(value):
    return "%.1f" % (value*100)
