from django import template
from django.core.paginator import Paginator

register = template.Library()

@register.simple_tag
def paginate_pages(pages, current_page):
    # maybe put number of pages on bottom in settings later
    if pages.num_pages <= 5:
        first = 0
        last = pages.count
    elif current_page < 3:
        first = 0
        last = 5
    elif current_page + 2 > pages.count:
        first = pages.count -5
        last = pages.count
    else:
        first = current_page - 3
        last = current_page + 2

    return pages.page_range[first:last]

