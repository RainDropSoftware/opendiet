from django.forms import ModelForm, DecimalField, DateField
from Log.models import Log


class LogEntry(ModelForm):
    class Meta:
        model = Log
        fields = ['log_date', 'weight', 'body_fat', 'note']

