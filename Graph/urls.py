from django.urls import path, re_path

from . import views

urlpatterns = [
	re_path(r'^$', views.graph, name='graph'),
	re_path(r'^$', views.graph, name='graph'),
	re_path(r'^(?P<days>\d+)$', views.graph, name='graph'),
	re_path(r'^(?P<username>\w+)$', views.graph, name='graph'),
	re_path(r'^(?P<username>\w+)/(?P<days>\d+)$', views.graph, name='graph'),
]
