from django.shortcuts import render
from django.contrib.auth.models import User
from Profile.models import UserInfo
from Log.models import Log
import datetime


def graph(request, days=30, username=''):
    """Graph placeholder"""
    days = int(days)
    print(days)
    if username == '':
        user = request.user
    else :
        try :
            user = User.objects.filter(username=username)[0]
        except IndexError:
            return render(request, 'graph.html', {'error': "User Does Not Exist or Has Not Shared Publicaly"})

    try :
        last_log = Log.objects.order_by('-log_date').filter(user=user)[0:1].get()
    except Log.DoesNotExist:
            return render(request, 'graph.html', {'error': "User has no log entries"})
    days_ago = last_log.log_date - datetime.timedelta(days=days)

    logs = last_log.entries_between_dates(days_ago, last_log.log_date)
    # Grab userinfo
    userinfo = UserInfo.objects.get(user=user)
   
    return render(request, 'graph.html', {'log': logs, 'metric': userinfo.metric, 'username': user.username})


